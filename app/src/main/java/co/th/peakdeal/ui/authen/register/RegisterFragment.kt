package co.th.peakdeal.ui.authen.register

import co.th.peakdeal.R
import co.th.peakdeal.base.BaseFragment
import co.th.peakdeal.databinding.FragmentRegisterBinding
import org.koin.androidx.viewmodel.ext.android.getViewModel

class RegisterFragment : BaseFragment<FragmentRegisterBinding, RegisterViewModel>() {

    override fun getLayout(): Int = R.layout.fragment_register

    override fun initViewModel(): RegisterViewModel = getViewModel()

    override fun setupView() {
        binding.viewModel = viewModel
    }

    override fun setObserve() {
    }

}