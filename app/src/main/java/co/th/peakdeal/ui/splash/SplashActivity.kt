package co.th.peakdeal.ui.splash

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import co.th.peakdeal.R

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        SplashRun(this,this)
    }
}
