package co.th.peakdeal.ui.splash

import android.content.Intent
import android.os.Handler
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent
import co.th.peakdeal.ui.AuthenActivity

class SplashRun(lifecycleOwner: LifecycleOwner, activity: SplashActivity) : LifecycleObserver {

    private var mDelayHandler: Handler? = null
    private val SPLASH_DELAY: Long = 3000 //3 seconds
    private val mRunnable: Runnable = Runnable {
        val intent = Intent(activity, AuthenActivity::class.java)
        activity.startActivity(intent)
        activity.finish()
    }

    init {
        lifecycleOwner.lifecycle.addObserver(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun splashRun() {
        mDelayHandler = Handler()
        mDelayHandler?.postDelayed(mRunnable, SPLASH_DELAY)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onSplashDestroy() {
        if (mDelayHandler != null) {
            mDelayHandler!!.removeCallbacks(mRunnable)
        }
    }
}