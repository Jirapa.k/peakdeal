package co.th.peakdeal.ui.authen.login

import co.th.peakdeal.R
import co.th.peakdeal.base.BaseFragment
import co.th.peakdeal.databinding.FragmentLoginBinding
import org.koin.androidx.viewmodel.ext.android.getViewModel

class LoginFragment : BaseFragment<FragmentLoginBinding, LoginViewModel>() {

    override fun getLayout(): Int = R.layout.fragment_login

    override fun initViewModel(): LoginViewModel = getViewModel()

    override fun setupView() {
        binding.viewModel = viewModel
    }

    override fun setObserve() {
    }

}