package co.th.peakdeal.ui.authen.forgotPassword

import co.th.peakdeal.R
import co.th.peakdeal.base.BaseFragment
import co.th.peakdeal.databinding.FragmentForgotPasswordBinding
import org.koin.androidx.viewmodel.ext.android.getViewModel

class ForgotPasswordFragment :
    BaseFragment<FragmentForgotPasswordBinding, ForgotPasswordViewModel>() {

    override fun getLayout(): Int = R.layout.fragment_forgot_password

    override fun initViewModel(): ForgotPasswordViewModel = getViewModel()

    override fun setupView() {
        binding.viewModel = viewModel
    }

    override fun setObserve() {
    }

}