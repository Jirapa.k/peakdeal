package com.th.shipmedis

import android.app.Application
import co.th.peakdeal.di.appModule
import com.github.nkzawa.socketio.client.IO
import com.github.nkzawa.socketio.client.Socket
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import java.net.URISyntaxException

class MainApplication : Application() {

    private var mSocket: Socket? = null

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@MainApplication)
            modules(appModule)
        }
    }

    fun getSocket(): Socket {
        try {
            mSocket = IO.socket("http://103.74.254.101")
        } catch (e: URISyntaxException) {
            throw RuntimeException(e)
        }
        return mSocket!!
    }
}