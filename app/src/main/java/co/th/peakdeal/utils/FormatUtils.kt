package co.th.peakdeal.utils

import android.util.Patterns

object FormatUtils {
    fun checkEmailFormat(text: String): Boolean {
        val pattern = Patterns.EMAIL_ADDRESS
        return pattern.matcher(text).matches()
    }
}