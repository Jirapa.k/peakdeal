package co.th.peakdeal.utils

import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.google.android.material.textfield.TextInputLayout

object AppBindingAdapter {

    @BindingAdapter("imageUrl")
    @JvmStatic
    fun ImageView.loadImage(url: String?) {
        url?.let {
            Glide.with(context).load(it).into(this)
        }
    }

//    @BindingAdapter("imageDrawable")
//    @JvmStatic
//    fun TextView.setImageResource(drawable: Int?) {
//        // this.setCompoundDrawablesWithIntrinsicBounds(drawable!!, 0, R.drawable.ic_next_black, 0)
//    }

    @BindingAdapter("emailValidate")
    @JvmStatic
    fun TextInputLayout.setOnChange(errorText: String) {
        this.editText?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {}
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (!FormatUtils.checkEmailFormat(s.toString())) {
                    isErrorEnabled = true
                    error = errorText
                    Log.d("test","yes")
                } else {
                    isErrorEnabled = false
                    error = ""
                    Log.d("test","no")
                }
            }
        })
    }
}